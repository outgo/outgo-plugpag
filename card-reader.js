var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
var PlugPagCardReader = /** @class */ (function () {
    function PlugPagCardReader() {
    }
    /**
     * Função que executa uma compra pelo cartão, usando o PlugPag.
     * @param data Informações sobre a compra a ser realizada.
     * @returns Um observável que informa cada etapa da compra sendo realizada.
     */
    PlugPagCardReader.prototype.purchase = function (data) {
        return new Observable(function (subscriber) {
            if (!window.plugPag) {
                console.warn("Plugin PlugPag não está instalado!");
                subscriber.complete();
            }
            else {
                var transactionId_1;
                var transactionCode_1;
                //Garantir que o objeto vai como string, para evitar erros.
                data.reference = "" + data.reference;
                window.plugPag.purchase(data, function (response) {
                    var code;
                    var message = response.message ? response.message.toLowerCase() : null;
                    if (response.transactionId)
                        transactionId_1 = response.transactionId;
                    if (response.transactionCode)
                        transactionCode_1 = response.transactionCode;
                    //Deduzindo o código baseado na mensagem enviada.
                    if (message == null) {
                        if (response.code == 17) {
                            code = PlugPagEventCode.EVENT_CODE_CLEAR_PIN;
                        }
                        else {
                            code = PlugPagEventCode.EVENT_CODE_INSERTED_KEY;
                        }
                    }
                    else if (message.startsWith("processando")) {
                        code = PlugPagEventCode.EVENT_CODE_DEFAULT;
                    }
                    else if (message.includes("insira")) {
                        code = PlugPagEventCode.EVENT_CODE_WAITING_CARD;
                    }
                    else if (message == "senha verificada") {
                        code = PlugPagEventCode.EVENT_CODE_PIN_OK;
                    }
                    else if (message == "pagamento finalizado") {
                        code = PlugPagEventCode.EVENT_CODE_SALE_END;
                    }
                    else if (message.startsWith("selecionado:")) {
                        code = PlugPagEventCode.EVENT_CODE_INSERTED_CARD;
                    }
                    else if (message.includes("senha:")) {
                        code = PlugPagEventCode.EVENT_CODE_PIN_REQUESTED;
                    }
                    else if (message.includes("senha incorreta")) {
                        code = PlugPagEventCode.EVENT_CODE_PIN_ERROR;
                    }
                    else if (message.startsWith("retire")) {
                        code = PlugPagEventCode.EVENT_CODE_WAITING_REMOVE_CARD;
                    }
                    else if (message == "venda não aprovada") {
                        code = PlugPagEventCode.EVENT_CODE_SALE_ERROR;
                    }
                    else if (message.startsWith("erro")) {
                        code = PlugPagEventCode.EVENT_CODE_CARD_ERROR;
                    }
                    else if (message.includes("chip")) {
                        code = PlugPagEventCode.EVENT_CODE_NO_SWIPE;
                    }
                    else {
                        code = PlugPagEventCode.EVENT_CODE_UNKNOWN;
                    }
                    response.code = code;
                    if (!response.transactionId)
                        response.transactionId = transactionId_1;
                    if (!response.transactionCode)
                        response.transactionCode = transactionCode_1;
                    subscriber.next(response);
                    if (response.transactionId) {
                        subscriber.complete();
                    }
                }, function (error) {
                    console.error(error);
                    subscriber.error(error);
                });
            }
        });
    };
    /**
     * Função que cancela uma compra em andamento.
     * @returns Uma promessa que resolve quando a operação for cancelada.
     */
    PlugPagCardReader.prototype.abort = function () {
        if (!window.plugPag) {
            var erro = "Plugin PlugPag não está instalado!";
            console.warn(erro);
            return Promise.resolve();
        }
        else {
            return new Promise(function (resolve, reject) {
                window.plugPag.abort(resolve, reject);
            });
        }
    };
    PlugPagCardReader = __decorate([
        Injectable({ providedIn: "root" })
    ], PlugPagCardReader);
    return PlugPagCardReader;
}());
export { PlugPagCardReader };
/**
 * Dados necessários para uma compra PlugPag.
 */
var PlugPagPaymentData = /** @class */ (function () {
    function PlugPagPaymentData() {
    }
    return PlugPagPaymentData;
}());
export { PlugPagPaymentData };
export var PlugPagPaymentType;
(function (PlugPagPaymentType) {
    PlugPagPaymentType[PlugPagPaymentType["CREDITO"] = 1] = "CREDITO";
    PlugPagPaymentType[PlugPagPaymentType["DEBITO"] = 2] = "DEBITO";
    PlugPagPaymentType[PlugPagPaymentType["VOUCHER"] = 3] = "VOUCHER";
})(PlugPagPaymentType || (PlugPagPaymentType = {}));
export var PlugPagInstallmentType;
(function (PlugPagInstallmentType) {
    PlugPagInstallmentType[PlugPagInstallmentType["A_VISTA"] = 1] = "A_VISTA";
    PlugPagInstallmentType[PlugPagInstallmentType["PARC_VENDEDOR"] = 2] = "PARC_VENDEDOR";
    PlugPagInstallmentType[PlugPagInstallmentType["PARC_COMPRADOR"] = 3] = "PARC_COMPRADOR";
})(PlugPagInstallmentType || (PlugPagInstallmentType = {}));
/**
 * Códigos enviados pelo pagamento via PlugPag.
 */
export var PlugPagEventCode;
(function (PlugPagEventCode) {
    /**
     * Código do evento não reconhecido pela biblioteca.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_UNKNOWN"] = -2] = "EVENT_CODE_UNKNOWN";
    /**
     * Código padrão de evento. Utilizado quando nenhum evento foi enviado.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_DEFAULT"] = -1] = "EVENT_CODE_DEFAULT";
    /**
     * Código de evento indicando que o leitor está aguardando o usuário inserir o cartão.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_WAITING_CARD"] = 0] = "EVENT_CODE_WAITING_CARD";
    /**
     * Código de evento indicando que o cartão foi inserido.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_INSERTED_CARD"] = 1] = "EVENT_CODE_INSERTED_CARD";
    /**
     * Código de evento indicando que o leitor está aguardando o usuário digitar a senha.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_PIN_REQUESTED"] = 2] = "EVENT_CODE_PIN_REQUESTED";
    /**
     * Código de evento indicando que a senha digitada foi validada com sucesso.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_PIN_OK"] = 3] = "EVENT_CODE_PIN_OK";
    /**
     * Código de evento indicando o fim da transação.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_SALE_END"] = 4] = "EVENT_CODE_SALE_END";
    /**
     * Código de evento indicando que o terminal está aguardando autorização da senha digitada para prosseguir com a transação.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_AUTHORIZING"] = 5] = "EVENT_CODE_AUTHORIZING";
    /**
     * Código de evento indicando que a senha foi digitada.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_INSERTED_KEY"] = 6] = "EVENT_CODE_INSERTED_KEY";
    /**
     * Código de evento indicando que o terminal está aguardando o usuário remover o cartão.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_WAITING_REMOVE_CARD"] = 7] = "EVENT_CODE_WAITING_REMOVE_CARD";
    /**
     * Código de evento indicando que o cartão foi removido do terminal.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_REMOVED_CARD"] = 8] = "EVENT_CODE_REMOVED_CARD";
    /**
     * Código de evento indicando que a senha digitada está incorreta.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_PIN_ERROR"] = 9] = "EVENT_CODE_PIN_ERROR";
    /**
     * Código de evento indicando que a transação não foi aprovada.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_SALE_ERROR"] = 10] = "EVENT_CODE_SALE_ERROR";
    /**
     * Código de evento indicando que a comunicação com o cartão foi interrompida.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_CARD_ERROR"] = 11] = "EVENT_CODE_CARD_ERROR";
    /**
     * Código de evento indicando que a transação não pode ser executada pela fita magnética.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_NO_SWIPE"] = 12] = "EVENT_CODE_NO_SWIPE";
    /**
     * Código de evento indicando que a senha foi apagada.
     */
    PlugPagEventCode[PlugPagEventCode["EVENT_CODE_CLEAR_PIN"] = 13] = "EVENT_CODE_CLEAR_PIN";
})(PlugPagEventCode || (PlugPagEventCode = {}));
var PlugPagPaymentResponse = /** @class */ (function () {
    function PlugPagPaymentResponse() {
    }
    return PlugPagPaymentResponse;
}());
export { PlugPagPaymentResponse };
