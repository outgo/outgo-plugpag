import { Observable } from "rxjs";
export declare class PlugPagCardReader {
    /**
     * Função que executa uma compra pelo cartão, usando o PlugPag.
     * @param data Informações sobre a compra a ser realizada.
     * @returns Um observável que informa cada etapa da compra sendo realizada.
     */
    purchase(data: PlugPagPaymentData): Observable<PlugPagPaymentResponse>;
    /**
     * Função que cancela uma compra em andamento.
     * @returns Uma promessa que resolve quando a operação for cancelada.
     */
    abort(): Promise<void>;
}
/**
 * Dados necessários para uma compra PlugPag.
 */
export declare class PlugPagPaymentData {
    /**
     * Tipo do pagamento. Pode ser crédito, debito ou voucher.
     */
    paymentType: PlugPagPaymentType;
    /**
     * O valor da compra, em centavos.
     */
    amountInCents: number;
    /**
     * Informação se a compra vai ser parcelada ou não.
     */
    installmentType: PlugPagInstallmentType;
    /**
     * Quantidade de parcelas na compra.
     */
    installmentCount: number;
    /**
     * Código de referência, para controle interno da aplicação.
     */
    reference: string;
    /**
     * Se `true`, imprime um comprovante para o comprador após a compra.
     */
    printReceipt: boolean;
}
export declare enum PlugPagPaymentType {
    CREDITO = 1,
    DEBITO = 2,
    VOUCHER = 3
}
export declare enum PlugPagInstallmentType {
    A_VISTA = 1,
    PARC_VENDEDOR = 2,
    PARC_COMPRADOR = 3
}
/**
 * Códigos enviados pelo pagamento via PlugPag.
 */
export declare enum PlugPagEventCode {
    /**
     * Código do evento não reconhecido pela biblioteca.
     */
    EVENT_CODE_UNKNOWN = -2,
    /**
     * Código padrão de evento. Utilizado quando nenhum evento foi enviado.
     */
    EVENT_CODE_DEFAULT = -1,
    /**
     * Código de evento indicando que o leitor está aguardando o usuário inserir o cartão.
     */
    EVENT_CODE_WAITING_CARD = 0,
    /**
     * Código de evento indicando que o cartão foi inserido.
     */
    EVENT_CODE_INSERTED_CARD = 1,
    /**
     * Código de evento indicando que o leitor está aguardando o usuário digitar a senha.
     */
    EVENT_CODE_PIN_REQUESTED = 2,
    /**
     * Código de evento indicando que a senha digitada foi validada com sucesso.
     */
    EVENT_CODE_PIN_OK = 3,
    /**
     * Código de evento indicando o fim da transação.
     */
    EVENT_CODE_SALE_END = 4,
    /**
     * Código de evento indicando que o terminal está aguardando autorização da senha digitada para prosseguir com a transação.
     */
    EVENT_CODE_AUTHORIZING = 5,
    /**
     * Código de evento indicando que a senha foi digitada.
     */
    EVENT_CODE_INSERTED_KEY = 6,
    /**
     * Código de evento indicando que o terminal está aguardando o usuário remover o cartão.
     */
    EVENT_CODE_WAITING_REMOVE_CARD = 7,
    /**
     * Código de evento indicando que o cartão foi removido do terminal.
     */
    EVENT_CODE_REMOVED_CARD = 8,
    /**
     * Código de evento indicando que a senha digitada está incorreta.
     */
    EVENT_CODE_PIN_ERROR = 9,
    /**
     * Código de evento indicando que a transação não foi aprovada.
     */
    EVENT_CODE_SALE_ERROR = 10,
    /**
     * Código de evento indicando que a comunicação com o cartão foi interrompida.
     */
    EVENT_CODE_CARD_ERROR = 11,
    /**
     * Código de evento indicando que a transação não pode ser executada pela fita magnética.
     */
    EVENT_CODE_NO_SWIPE = 12,
    /**
     * Código de evento indicando que a senha foi apagada.
     */
    EVENT_CODE_CLEAR_PIN = 13
}
export declare class PlugPagPaymentResponse {
    message: string;
    code: PlugPagEventCode;
    transactionId: string;
    transactionCode: string;
    serial: string;
}
