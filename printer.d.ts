export declare class PlugPagPrinter {
    /**
     * Imprime um imagem, fornecida em base64, pelo plugin da PlugPag.
     * @param image A imagem, em base64.
     * @returns Uma promessa que resolve quando a imagem for enviada para impressão.
     */
    printImage(image: string): Promise<void>;
    /**
     * Imprime um texto pelo plugin da PlugPag.
     * @param text O texto a ser impresso.
     * @returns Uma promessa que resolve quando o texto for enviado para impressão.
     */
    printText(...text: ({
        text?: string;
        font: {
            name: string;
            maxLength: number;
        };
        ignore?: boolean;
        image?: string;
    })[]): Promise<void>;
    /**
     * Método que adiciona uma imagem no graphics.
     * @param imageURL URL da imagem a ser inserida no graphics.
     * @param graphics O objeto graphics do canvas.
     * @param imageWidth A largura e altura da imagem a ser inserida.
     * @param currentLine A linha atual sendo preenchida.
     * @returns Uma promessa que retorna a linha atual a ser preenchida, após a imagem.
     */
    private addImageToGraphic;
}
