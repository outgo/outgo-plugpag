var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from "@angular/core";
import * as QRCodeGenerator from "qrcode";
var PlugPagPrinter = /** @class */ (function () {
    function PlugPagPrinter() {
    }
    /**
     * Imprime um imagem, fornecida em base64, pelo plugin da PlugPag.
     * @param image A imagem, em base64.
     * @returns Uma promessa que resolve quando a imagem for enviada para impressão.
     */
    PlugPagPrinter.prototype.printImage = function (image) {
        if (!window.plugPag) {
            console.warn("Plugin PlugPag não esta instalado!");
            return Promise.resolve();
        }
        else {
            return new Promise(function (resolve, reject) {
                window.plugPag.print(image, resolve, reject);
            });
        }
    };
    /**
     * Imprime um texto pelo plugin da PlugPag.
     * @param text O texto a ser impresso.
     * @returns Uma promessa que resolve quando o texto for enviado para impressão.
     */
    PlugPagPrinter.prototype.printText = function () {
        var text = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            text[_i] = arguments[_i];
        }
        return __awaiter(this, void 0, void 0, function () {
            var graphics, lineHeight, charactersPerLine, qrCodeWidth, bonusHeight, currentLine, i, line, words, phrase1, phrase2, word, i, imageURL, line, qrcode, qrcodeImage, image;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!window.plugPag) return [3 /*break*/, 1];
                        console.warn("Plugin PlugPag não esta instalado!");
                        return [2 /*return*/];
                    case 1:
                        graphics = document.createElement("canvas").getContext("2d");
                        lineHeight = 40;
                        charactersPerLine = 18;
                        qrCodeWidth = 280;
                        bonusHeight = 0;
                        //Remover linhas ignoradas
                        text = text.filter(function (line) { return !line.ignore; });
                        currentLine = lineHeight;
                        for (i = 0; i < text.length; i++) {
                            line = text[i].text;
                            charactersPerLine = text[i].font.maxLength;
                            //Identificar tags
                            if (text[i].image || (line && line.startsWith("<qrcode>"))) {
                                bonusHeight += qrCodeWidth;
                                continue;
                            }
                            if (!line)
                                continue;
                            while (line.length > charactersPerLine) {
                                words = line.split(" ");
                                phrase1 = void 0, phrase2 = void 0;
                                if (words.length == 1) {
                                    word = words[0];
                                    phrase1 = word.substring(0, word.length / 2);
                                    phrase2 = word.substring(word.length / 2, word.length);
                                }
                                else {
                                    phrase1 = words.shift();
                                    phrase2 = words.pop();
                                    while (words.length > 0) {
                                        if (phrase1.length > phrase2.length) {
                                            phrase2 = words.pop() + " " + phrase2;
                                        }
                                        else {
                                            phrase1 += " " + words.shift();
                                        }
                                    }
                                }
                                line = phrase1;
                                text.splice(i + 1, 0, { text: phrase2, font: text[i].font });
                            }
                            // let spacesNeeded = charactersPerLine - line.length;
                            // if (spacesNeeded > 0) {
                            //    line = " ".repeat(spacesNeeded/2) + line;
                            // }
                            text[i].text = line;
                        }
                        graphics.canvas.width = 450;
                        graphics.canvas.height = (lineHeight * text.length) + bonusHeight;
                        graphics.fillStyle = "white";
                        graphics.fillRect(0, 0, graphics.canvas.width, graphics.canvas.height);
                        graphics.fillStyle = "black";
                        graphics.textAlign = "center";
                        i = 0;
                        _a.label = 2;
                    case 2:
                        if (!(i < text.length)) return [3 /*break*/, 10];
                        if (!text[i].image) return [3 /*break*/, 4];
                        imageURL = text[i].image;
                        return [4 /*yield*/, this.addImageToGraphic(imageURL, graphics, qrCodeWidth, currentLine)];
                    case 3:
                        currentLine = _a.sent();
                        return [3 /*break*/, 9];
                    case 4:
                        line = text[i].text;
                        graphics.font = text[i].font.name;
                        if (!line) return [3 /*break*/, 8];
                        if (!line.startsWith("<qrcode>")) return [3 /*break*/, 7];
                        qrcode = line.replace("<qrcode>", "").replace("</qrcode>", "");
                        return [4 /*yield*/, QRCodeGenerator.toDataURL(qrcode, {
                                width: qrCodeWidth
                            })];
                    case 5:
                        qrcodeImage = _a.sent();
                        return [4 /*yield*/, this.addImageToGraphic(qrcodeImage, graphics, qrCodeWidth, currentLine)];
                    case 6:
                        currentLine = _a.sent();
                        // let spacesNeeded = charactersPerLine - qrcode.length;
                        // if (spacesNeeded > 0) {
                        //    qrcode = " ".repeat(spacesNeeded/2) + qrcode;
                        // }
                        // graphics.fillText(qrcode, 10, currentLine);
                        // graphics.font = "bold 20px Roboto";
                        graphics.fillText(qrcode.trim(), graphics.canvas.width / 2, currentLine);
                        console.log("qrcode impresso", qrcode, qrcode.trim());
                        return [3 /*break*/, 8];
                    case 7:
                        // if (i == 0) {
                        //    graphics.font = "bold 40px Roboto";
                        // } else if (i == 2) {
                        //    graphics.font = "bold 35px Roboto";
                        // } else if (i % 3 == 0) {
                        //    graphics.font = "bold 30px Roboto";
                        // } else {
                        //    graphics.font = "bold 25px Roboto";
                        // }
                        graphics.fillText(line.trim(), graphics.canvas.width / 2, currentLine);
                        console.log("linha impressa", line, line.trim());
                        _a.label = 8;
                    case 8:
                        currentLine += lineHeight;
                        _a.label = 9;
                    case 9:
                        i++;
                        return [3 /*break*/, 2];
                    case 10:
                        ;
                        image = graphics.canvas.toDataURL().split("base64,")[1];
                        return [2 /*return*/, this.printImage(image)];
                }
            });
        });
    };
    /**
     * Método que adiciona uma imagem no graphics.
     * @param imageURL URL da imagem a ser inserida no graphics.
     * @param graphics O objeto graphics do canvas.
     * @param imageWidth A largura e altura da imagem a ser inserida.
     * @param currentLine A linha atual sendo preenchida.
     * @returns Uma promessa que retorna a linha atual a ser preenchida, após a imagem.
     */
    PlugPagPrinter.prototype.addImageToGraphic = function (imageURL, graphics, imageWidth, currentLine) {
        return __awaiter(this, void 0, void 0, function () {
            var qrcodePromise;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        qrcodePromise = new Promise(function (resolve, reject) {
                            var image = new Image();
                            image.crossOrigin = "anonymous";
                            image.onload = function () {
                                graphics.drawImage(image, (graphics.canvas.width - imageWidth) / 2, currentLine - 10, imageWidth, imageWidth);
                                resolve();
                            };
                            image.src = imageURL;
                        });
                        return [4 /*yield*/, qrcodePromise];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, currentLine + imageWidth];
                }
            });
        });
    };
    PlugPagPrinter = __decorate([
        Injectable({ providedIn: "root" })
    ], PlugPagPrinter);
    return PlugPagPrinter;
}());
export { PlugPagPrinter };
